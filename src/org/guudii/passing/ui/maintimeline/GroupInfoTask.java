package org.guudii.passing.ui.maintimeline;

import org.guudii.passing.bean.GroupListBean;
import org.guudii.passing.dao.maintimeline.FriendGroupDao;
import org.guudii.passing.support.database.GroupDBTask;
import org.guudii.passing.support.error.WeiboException;
import org.guudii.passing.support.lib.MyAsyncTask;
import org.guudii.passing.support.utils.GlobalContext;

/**
 * User: qii
 * Date: 12-12-28
 */
public class GroupInfoTask extends MyAsyncTask<Void, GroupListBean, GroupListBean> {


    private WeiboException e;

    private String token;
    private String accountId;

    public GroupInfoTask(String token, String accountId) {
        this.token = token;
        this.accountId = accountId;
    }

    @Override
    protected GroupListBean doInBackground(Void... params) {
        try {
            return new FriendGroupDao(token).getGroup();
        } catch (WeiboException e) {
            this.e = e;
            cancel(true);
        }
        return null;
    }


    @Override
    protected void onPostExecute(GroupListBean groupListBean) {
        super.onPostExecute(groupListBean);

        GroupDBTask.update(groupListBean, accountId);
        if (accountId.equalsIgnoreCase(GlobalContext.getInstance().getCurrentAccountId()))
            GlobalContext.getInstance().setGroup(groupListBean);

    }

}