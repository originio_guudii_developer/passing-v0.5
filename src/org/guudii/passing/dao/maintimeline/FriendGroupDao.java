package org.guudii.passing.dao.maintimeline;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.guudii.passing.bean.GroupListBean;
import org.guudii.passing.dao.URLHelper;
import org.guudii.passing.support.error.WeiboException;
import org.guudii.passing.support.http.HttpMethod;
import org.guudii.passing.support.http.HttpUtility;
import org.guudii.passing.support.debug.AppLogger;

import java.util.HashMap;
import java.util.Map;

/**
 * User: qii
 * Date: 12-10-17
 */
public class FriendGroupDao {


    public GroupListBean getGroup() throws WeiboException {

        String url = URLHelper.FRIENDSGROUP_INFO;

        Map<String, String> map = new HashMap<String, String>();
        map.put("access_token", access_token);

        String jsonData = HttpUtility.getInstance().executeNormalTask(HttpMethod.Get, url, map);

        Gson gson = new Gson();

        GroupListBean value = null;
        try {
            value = gson.fromJson(jsonData, GroupListBean.class);
        } catch (JsonSyntaxException e) {
            AppLogger.e(e.getMessage());
        }


        return value;
    }


    public FriendGroupDao(String token) {
        this.access_token = token;
    }

    private String access_token;
}
